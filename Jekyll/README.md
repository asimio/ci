### What's this?

This is a Jekyll Docker image used to build https://tech.asimio.net and deploy it to Amazon S3.

### Usage
Example usage in bitbucket-pipelines.yml file:
```
image: asimio/ci-jekyll:1.11.9

pipelines:
  branches:
    master:
      - step:
          script:
            - chmod o+t /tmp
            - ruby -v && jekyll -version && bundler -v
            - ALGOLIA_API_KEY=<Admin API Key> ALGOLIA_INDEX_NAME=<env-index-name> bundle exec jekyll algolia
            - JEKYLL_ENV=<environment> bundle exec jekyll build
            - s3_website push

```

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero